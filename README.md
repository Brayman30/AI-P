
# AI&P
 My personal **A**pp **I**nstall **&** **P**ersonalisation script 

<a href="https://github.com/Brayman30/AI-P#how-do-i-use-it"><img src="https://i.ibb.co/5YYY9rT/button-instructions-to-use-this-script.png"</a>

## OK, but what's it really

It's a simple shell script which installs many things, including the following:

 - Nano
 - Firefox
 - VSCode
 - Discord
 - GParted
 - GIMP
 - Thunar
 - Python
 - qBittorent
 - Variety

And, also changes Variety to 
## How do I use it?
Simple*...
Open a terminal window and type 

    git clone https://github.com/Brayman30/AI-P.git

Now, type 

    cd AI-P

Now... Here's where things get a **little *messy***, 
depending on which of distribution you are using, the commands are a *little different* but, no big deal, files have already been created for Debian, Arch, Red Hat, & OpenSUSE (Hint: if you don't know what you are using, your probably using Debian.)
#### The following commands will apply to each of those OS:
|Distro|Command|
|--|--|
|Debian|`bash Debian-AI-P.sh`|
|Arch|`bash Arch-AI-P.sh`|
|Red Hat|`bash REL-AI-P.sh`|
|OpenSUSE|`bash SUSE-AI-P.sh`|

Once you start the script, you will be asked for your password either by a fancy GUI (if you have zenity installed) or by a sudo prompt
### *You will need to have Git installed already... but, if you don't, no worries... 
just do

    sudo apt-get install git

***or*** replace **apt-get** with your package manager of choice.

If you ***Really*** don't want to install Git, just follow this link and click the green code button and then download Zip, then you will have to unzip the zipped file that your web browser downloads.
#### Also, this will only apply to a very limited number of you, but if you don't have bash, you can't use this... simple as that
